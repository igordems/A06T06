/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a06t06;

import javax.swing.JOptionPane;

/**
 *
 * @author Igor
 */
public class Calendario {
    public static void semana(int dia, int tipo){
        String nomedia = semana(1);
        switch (tipo) {
            case 1:
                System.out.println(nomedia);
                break;
            case 2:
                JOptionPane.showMessageDialog(null, nomedia);
                break;
            default:
                System.out.println("tipo invalido");
                break;
        }
    }
    public static String semana(int dia){
        String nomedia;
        switch (dia) {
            case 1:
                nomedia = "Domingo";
                break;
            case 2:
                nomedia = "Segunda-feira";
                break;
            case 3:
                nomedia = "Terça-feira";
                break;
            case 4:
                nomedia = "Quarta-feira";
                break;
            case 5:
                nomedia = "Quinta-feira";
                break;
            case 6:
                nomedia = "Sexta-feira";
                break;
             case 7:
                nomedia = "Sábado";
                break;
            default:
                nomedia = "Este não é um dia válido!";
         }
        return nomedia;
    }
}
